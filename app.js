let id_editar = window.location.search
id_editar = id_editar.replace('?', '')

veiculosVendidos = Array()
let vendido

function vai(){
    console.log(vendido)
    console.log(veiculosVendidos)
}

class Veiculo{
    constructor(modelo, marca, descricao,valor){
        this.modelo = modelo
        this.marca = marca
        this.descricao = descricao
        this.valor = valor
        this.img = img_aleatorio()
        this.id = bd.getProximoId()
    }
    validarDados(){ // validar dados na hr de cadastra uma nova despesa

        for(let i in this){
            if(this[i] == undefined || this[i] == '' || this[i] == null){
                return false
            }
        }
        return true
    }
}
class BD{
    getProximoId(){
        let testeId = localStorage.getItem('id')

        if(testeId === null){
            localStorage.setItem('id', 0)
        }
        let proximoId = localStorage.getItem('id')

        return parseInt(proximoId) + 1
    }
    getProximoIdVenda(){
        let testeId = localStorage.getItem('idVenda')

        if(testeId === null){
            localStorage.setItem('idVenda', 0)
        }
        let proximoId = localStorage.getItem('idVenda')

        return parseInt(proximoId) + 1
    }
    gravarVeiculo(veiculo){
        let id =  this.getProximoId()

        localStorage.setItem(id, JSON.stringify(veiculo))

        localStorage.setItem('id', id)
    }
    gravarVeiculoVendido(vendido){
        let idVenda = this.getProximoIdVenda()

        localStorage.setItem('idVenda'+ idVenda, JSON.stringify(vendido))

        localStorage.setItem('idVenda', idVenda)
    }
    recuperarTodosOsVeiculos(){
        // arraya veiculos
        let veiculos = Array()
        let id = localStorage.getItem('id')
        if(id == null){
            return null
        }
        for(let i = 1; i <= id; i++){
            let veiculo = JSON.parse(localStorage.getItem(i))

            //se existe a posibilidade de ter um veiculo que foi removido           
            //neste caso vamos pular esse indice
            if(veiculo == null){
                continue
            }
            veiculo.id = i
            veiculos.push(veiculo)
        }
        return veiculos
    }
    recuperarTodosOsVeiculosVendidos(){
        // arraya veiculos
        let veiculos = Array()
        let id = localStorage.getItem('idVenda')
        if(id == null){
            return null
        }
        for(let i = 1; i <= id; i++){
            let veiculo = JSON.parse(localStorage.getItem('idVenda'+i))

            //se existe a posibilidade de ter um veiculo que foi removido           
            //neste caso vamos pular esse indice
            if(veiculo == null){
                continue
            }
            veiculos.push(veiculo)
        }
        return veiculos
    }
    vender(id){
        let vendido = JSON.parse(localStorage.getItem(id))
        this.gravarVeiculoVendido(vendido)

        localStorage.removeItem(id)
        dialogo.dialogoVenda()

    }
    excluirVeiculo(id){
        localStorage.removeItem(id)
        dialogo.dialogoExclusao()
    }
    campoEditarVeiculo(){

        let veiculo = JSON.parse(localStorage.getItem(id_editar))
        
        console.log(veiculo)
        document.getElementById('modelo').value = veiculo.modelo
        document.getElementById('marca').value = veiculo.marca
        document.getElementById('descricao').value = veiculo.descricao
        document.getElementById('valor').value = veiculo.valor
    }
    editarVeiculo(){
        let veiculo = JSON.parse(localStorage.getItem(id_editar))

        veiculo.modelo = document.getElementById('modelo').value
        veiculo.marca = document.getElementById('marca').value
        veiculo.descricao = document.getElementById('descricao').value
        veiculo.valor = document.getElementById('valor').value

        for(let i in veiculo){
            if(veiculo[i] == undefined || veiculo[i] == '' || veiculo[i] == null){
                dialogo.dialogoErro()

            }else{
            localStorage.setItem(id_editar, JSON.stringify(veiculo))
            dialogo.dialogoSucessoEdicao()
            }
        }
    }

} // final do objeto bd
bd = new BD()

class Dialogo{
    dialogoSucessoCadastro(){

        //dialogo sucesso
        document.getElementById('text-cor').className = 'modal-header text-success'
        document.getElementById('modal-title').innerHTML = 'Gravação realizada com sucesso'
        document.getElementById('modal-descricao').innerHTML ='Veiculo cadastratada com sucesso'
        document.getElementById('modal-button').className = 'btn btn-success'
        $('#modal').modal('show')
 
        // limpa o campo
        modelo.value = ''
        marca.value = ''
        descricao.value = ''
        valor.value = ''
    }
    dialogoErro(){
        //dialogo erro
        document.getElementById('text-cor').className =  'modal-header text-danger'
        document.getElementById('modal-title').innerHTML = 'Erro na Gravação'
        document.getElementById('modal-descricao').innerHTML ='Existem campos obrigatórios que não foram preemchidos'
        document.getElementById('modal-button').className = 'btn btn-danger'     
        $('#modal').modal('show')
    }
    dialogoSucessoEdicao(){
         //dialogo sucesso
         document.getElementById('text-cor').className = 'modal-header text-success'
         document.getElementById('modal-title').innerHTML = 'Gravação realizada com sucesso'
         document.getElementById('modal-descricao').innerHTML ='Veiculo editado com sucesso'
         document.getElementById('modal-button').className = 'btn btn-success'
         $('#modal').modal('show')
  
         // limpa o campo
         modelo.value = ''
         marca.value = ''
         descricao.value = ''
         valor.value = ''
    }
    dialogoExclusao(){
            //dialogo erro
            document.getElementById('text-cor').className =  'modal-header text-danger'
            document.getElementById('modal-title').innerHTML = 'Exclusão'
            document.getElementById('modal-descricao').innerHTML ='Veiculo Excluido com sucesso'
            document.getElementById('modal-button').className = 'btn btn-danger'     
            $('#modal').modal('show')
    }
    dialogoVenda(){
        document.getElementById('text-cor').className = 'modal-header text-success'
        document.getElementById('modal-title').innerHTML = 'Venda'
        document.getElementById('modal-descricao').innerHTML ='Veiculo vendido com sucesso'
        document.getElementById('modal-button').className = 'btn btn-success'
        $('#modal').modal('show')
    }
}
dialogo = new Dialogo()

function cadastraVeiculo(){
    let modelo = document.getElementById('modelo')
    let marca =  document.getElementById('marca')
    let descricao = document.getElementById('descricao')
    let valor = document.getElementById('valor')

    veiculo = new Veiculo(modelo.value, marca.value, descricao.value, valor.value)
    console.log(veiculo)
  
    if(veiculo.validarDados()){ 
        bd.gravarVeiculo(veiculo)
        dialogo.dialogoSucessoCadastro()
    }else{ 
        dialogo.dialogoErro()
    }
}

function cancelar(){
        window.location.href = 'index.html'
    }

function criarVeiculoHtml(){
    let veiculos = Array()
    veiculos = bd.recuperarTodosOsVeiculos()
    let row = document.getElementById('row_veiculos')
    console.log(veiculos)
    if( veiculos === undefined || veiculos === null || veiculos.length === 0){
        document.getElementById('footer').className = 'vai-pra-baixo'
        document.getElementById('h2').innerText = 'Ainda não existem veiculos para venda, por favor cadastre os.'
    }else{
        veiculos.forEach(function(v){ 
        row.innerHTML += `<div class="col-md-6 col-lg-4"> <div class="card"> <img class="card-img-top" src="${v.img}" height="200"/> <div class="card-body"><h4 class="card-title"> ${v.modelo} </h4> <h6 class="card-subtitle mb-2 text-muted"> ${v.marca} </h6> <p class="card-text"> ${v.descricao}. Por apenas: ${v.valor} R$ </p> <button type="button" class="btn btn-outline-primary" onclick = "bd.vender(${v.id})"> Vender </button> <button onclick="bd.excluirVeiculo(${v.id})" type="button" class="btn btn-outline-danger float-right"> <i class="far fa-trash-alt"></i> </button> <button onclick="irParaEditar(${v.id})" type="button" class="btn btn-outline-warning mr-3 float-right"> <i class=" far fa-edit "></i> </button> </div></div></div>`                 
    })
    }
}

function img_aleatorio(){
    let aleatorio = Math.random() * 4
    console.log(aleatorio)
    if(aleatorio < 1){
        return 'imagens/corsinha.jpg'
    }else if(aleatorio >= 1 && aleatorio < 2){
        return 'imagens/droga_e_brian.jpg'
    }else if(aleatorio >= 2 && aleatorio <3){
        return 'imagens/fusca.jpg'
    }else if( aleatorio >= 3 && aleatorio < 4){
        return 'imagens/fiat_147.jpg'
    }else{
        return 'imagens/lamborghini_venono.jpg'
    }
}
function irParaEditar(id){
    window.location.href = 'editar.html?'+id 
}
function teste_editar(){
    if(id_editar != null  &&  id_editar != ''  &&  id_editar != undefined){
        console.log(id_editar)
        bd.campoEditarVeiculo()
    }
}
function gerarTabelaVeiculosVendidos(){
    
    veiculosVendidos = bd.recuperarTodosOsVeiculosVendidos()
    console.log(veiculosVendidos)
    let tabela = document.getElementById('tabela')
    tabela.innerHTML = ''

    veiculosVendidos.forEach(function(v){
        // criando linhas
        let linha = tabela.insertRow()
        // criar as colunas
        linha.insertCell(0).innerHTML = v.id
        linha.insertCell(1).innerHTML = v.marca
        linha.insertCell(2).innerHTML = v.modelo
        linha.insertCell(3).innerHTML = v.valor
    })

}


